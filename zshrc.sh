# If you come from bash you might have to change your $PATH.
 export PATH=$HOME/bin:/usr/local/bin:/usr/local/sbin:$PATH

# Python env
  source ~/virtualenv/pesummary/bin/activate
# Path to your oh-my-zsh installation.
  export ZSH=".oh-my-zsh"
  #MY
#source $ZSH/custom/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 
#source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
plugins=(history-substring-search)
plugins=(zsh-autosuggestions)
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=4'
ZSH_AUTOSUGGEST_STRATEGY=match_prev_cmd
#source ~/SOFT/cWB/library/install/docker_watenv.sh
#source /home/cWB_docker/git/cWB/library/docker_watenv.sh
hash -d g=/home/cWB_docker/git
hash -d cg=~g/cWB/library


# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"
alias lrt="ls -lrt"
alias ll="ls -l --color=tty"
alias ls="ls --color=tty"
alias td='echo $(date +%Y-%m-%d)'

#Exists checks
_exists() { (( $+commands[$1] )) }

_exists vim      && export EDITOR=vim
_exists less     && export PAGER=less
_exists bsdtar   && alias tar='bsdtar'
_exists htop     && alias top='htop'

if _exists vim; then
    alias vim="vim -p"
    alias vi="vim"
fi

# Sourcing of the default watenv script: this needs to go before the root check!
source ~cg/docker_watenv.sh  
#alias scg="source ~cg/franz_watenv.sh"

if _exists root; then
    alias broot='root -b -l ' 
    alias brooth='date | tee -a README; tee -a README | root -b -l | tee -a README'
fi 


grepe() {
    vim +'/\v'"$1" +':silent tabdo :1 | normal! n' +':tabfirst' -p $(grep "$@" | cut -d: -f1 | sort -u)
}


unfunction _exists




# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
#ZSH_THEME="robbyrussell"
#ZSH_THEME="agnoster"
function set_win_title(){
    echo -ne "\033]0; $CWB_DOCKER_VERSION \007"
}
precmd_functions+=(set_win_title)
eval "$(starship init zsh)"
#ZSH_THEME="lima-agnoster"
#ZSH_THEME="myagnoster"
#ZSH_THEME="modagnoster"
#ZSH_THEME="candy"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
 CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13


# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"


alias kroot='pidof root | xargs kill -9 2> /dev/null;pidof root.exe | xargs kill -9 2> /dev/null'
alias tconv='lalapps_tconvert'

# Play safe!
alias 'rm=rm -i'
alias 'mv=mv -i'
alias 'cp=cp -i'

# For convenience
alias 'mkdir=mkdir -p'
alias 'dus=du -ms * | sort -n'

# Typing errors...
alias 'cd..=cd ..'

##To allow for the autocompletion of inline commands with up and down buttons
##bindkey "^[[A" history-beginning-search-backward
##bindkey "^[[B" history-beginning-search-forward
#[[ -n "${key[Up]}"      ]] && bindkey  "${key[Up]}"      history-beginning-search-backward
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

[[ -n "${key[Up]}"   ]] && bindkey "${key[Up]}"   up-line-or-beginning-search
[[ -n "${key[Down]}" ]] && bindkey "${key[Down]}" down-line-or-beginning-search

#Remembering recent directories
autoload -Uz chpwd_recent_dirs cdr add-zsh-hook
add-zsh-hook chpwd chpwd_recent_dirs

# Quick find
f() {
    echo "find . -iname \"*$1*\""
    find . -iname "*$1*"
}


#/MY

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
)

source $ZSH/oh-my-zsh.sh

# User configuration

 export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
 export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
 if [[ -n $SSH_CONNECTION ]]; then
   export EDITOR='vim'
 else
   export EDITOR='vim.tiny'
 fi

# Compilation flags
 export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"
#

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
# Quick ../../..
rationalise-dot() {
    if [[ $LBUFFER = *.. ]]; then
        LBUFFER+=/..
    else
        LBUFFER+=.
    fi
}
zle -N rationalise-dot
bindkey . rationalise-dot

autoload zsh/sched
#

